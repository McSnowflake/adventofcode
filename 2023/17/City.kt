import java.io.File

class City(private val grid: Array<IntArray>) {

    private val visited = Visited()
    private val unvisited = Unvisited()

    fun getPossibleNextBlocksFrom(block: Block, strategy: PathFindingStrategy): Map<Position, Vector> =
        Vector.AllDirections
            .filterNot { strategy.isNeighborForbidden(block, it) }
            .associateBy { block.position + it }
            .filterKeys { it.isInBounds(grid) }

    fun setStartingBlock(block: Block) = unvisited.add(block)

    fun hasMoreUnvisitedBlocks() = unvisited.hasMoreBlocks()

    fun visitBlockWithSmallestDistance(): Block {
        val block = unvisited.getBlockWithSmallestDistance()
        visited.add(block)
        return block
    }

    fun update(newPosition: Position, step: Vector, current: Block) {
        val newValue = current.heatLoss + grid[newPosition.y][newPosition.x]
        val newVector = if (current.direction.abs == step) current.direction + step else step

        if (!visited.hasBlockBeenVisited(newPosition, newVector))
            unvisited.update(Block(newPosition, newVector, newValue))
    }

    fun printTotalHeatLoss() = visited.printTotalHeatLoss(Position(grid.first().size - 1, grid.size - 1))

    companion object {
        fun scanFrom(map: File) = with(map.useLines {
            it.toList().map { it.toList().map { it.digitToInt() }.toIntArray() }.toTypedArray()
        }) {

            City(this)

        }
    }
}