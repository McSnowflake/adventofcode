data class Position(val x: Int, val y: Int) {
    operator fun plus(vector: Vector) = Position(x + vector.x, y + vector.y)
    fun isInBounds(array: Array<IntArray>): Boolean =
        x >= 0 && x < array.first().size &&
                y >= 0 && y < array.size
}