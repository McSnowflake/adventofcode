interface PathFindingStrategy {

    fun isNeighborForbidden(block: Block, vector: Vector): Boolean

    object NormalCrucible : PathFindingStrategy {
        override fun isNeighborForbidden(block: Block, vector: Vector): Boolean =

            // do not allow opposite direction ...
            block.direction.abs * -1 == vector ||

                    // ... nor more than 3 steps in one direction
                    (block.direction.length >= 3.0 && block.direction.abs == vector)

    }

    object UltraCrucible : PathFindingStrategy {
        override fun isNeighborForbidden(block: Block, vector: Vector): Boolean =

            // do not allow opposite direction ...
            block.direction.abs * -1 == vector ||

                    // ... nor more than 10 ...
                    (block.direction.length >= 10.0 && block.direction.abs == vector) ||

                    // ... or turning before having 4 steps in one direction (exception for starting block)
                    (block.direction.length <= 3.0 && (block.direction.abs != vector && block.direction.abs != Vector.Null))


    }
}