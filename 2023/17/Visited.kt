class Visited {

    private val blocks = mutableMapOf<Position, MutableMap<Vector, Int>>()
    fun add(block: Block) =
        if (blocks.containsKey(block.position)) blocks[block.position]!![block.direction] = block.heatLoss
        else blocks[block.position] = mutableMapOf(block.direction to block.heatLoss)


    fun hasBlockBeenVisited(newPosition: Position, newVector: Vector): Boolean =
        blocks[newPosition]?.containsKey(newVector) == true

    fun printTotalHeatLoss(position: Position) {
        println("-> total heat loss is ${blocks[position]!!.minBy { it.value }.value}")
    }


}