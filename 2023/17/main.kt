import PathFindingStrategy.NormalCrucible
import PathFindingStrategy.UltraCrucible
import java.io.File

fun main() {

    val map = File("2023/17/map")

    arrayOf(NormalCrucible, UltraCrucible).forEach { strategy ->
        findBestPath(map, strategy)
    }
}

fun findBestPath(map: File, strategy: PathFindingStrategy) {

    println("## strategy: $strategy")

    val city = City.scanFrom(map)
    city.setStartingBlock(Block(Position(0, 0), Vector.Null, 0))

    while (city.hasMoreUnvisitedBlocks()) {

        val currentBlock = city.visitBlockWithSmallestDistance()
        val nextBlocks = city.getPossibleNextBlocksFrom(currentBlock, strategy)
        nextBlocks.forEach { (position, vector) -> city.update(position, vector, currentBlock) }

     }

    city.printTotalHeatLoss()
}

