class Unvisited {

    private val blocks = mutableMapOf<Pair<Position, Vector>, Int>()

    fun hasMoreBlocks(): Boolean {
        //println("->   # of unvisited blocks  ${blocks.size}")
        return blocks.isNotEmpty()
    }

    fun add(block: Block) =
        if (!blocks.containsKey(block.combinedKey))
            blocks[block.combinedKey] = block.heatLoss
        else throw Exception("$block has already been added to unvisited ")

    fun getBlockWithSmallestDistance(): Block {

        // find block with the smallest distance
        val block = blocks.minBy { it.value }.key

        // remove it and retrieve the heat loss
        val heatLoss = blocks.remove(block)!!

        return Block(block.first, block.second, heatLoss)
    }

    fun update(block: Block) {
        val key = Pair(block.position, block.direction)
        if (!blocks.containsKey(key) || blocks[key]!! > block.heatLoss) blocks[key] = block.heatLoss
    }
}