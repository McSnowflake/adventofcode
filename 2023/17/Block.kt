data class Block(
    val position: Position,
    val direction: Vector,
    val heatLoss: Int
) {
    val combinedKey = Pair(position, direction)
}