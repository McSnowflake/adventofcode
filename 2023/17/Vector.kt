import kotlin.math.pow
import kotlin.math.sign
import kotlin.math.sqrt

data class Vector(val x: Int, val y: Int) {
    val abs: Vector
        get() = Vector(x.sign, y.sign)
    val length: Double
        get() = sqrt(x.toDouble().pow(2) + y.toDouble().pow(2))

    operator fun plus(another: Vector) = Vector(x + another.x, y + another.y)
    operator fun times(factor: Int) = Vector(x * factor, y * factor)



    companion object {
        val Null = Vector(0, 0)
        val AllDirections = setOf(
            Vector(0, 1),
            Vector(0, -1),
            Vector(-1, 0),
            Vector(1, 0)
        )
    }
}